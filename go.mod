module bitbucket.org/naufalandika/project-training

go 1.15

require (
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/labstack/echo/v4 v4.5.0 // indirect
	gorm.io/driver/mysql v1.1.1 // indirect
	gorm.io/gorm v1.21.12 // indirect
)
